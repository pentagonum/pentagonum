# Pentagonum

Pentagonum is a lamp setup with custom build lamps with lots of WS2812B leds powered by a custom build controllers.

## Controller

* 1 OrangePie Zero (Runs [Buildroot-Artnet-Serial](https://gitlab.com/pentagonum/buildroot-artnet-serial) with [Artnet-Serial](https://gitlab.com/pentagonum/artnet-serial)
* 4 ESP8266 (Each run [Serial-WS2812B](https://gitlab.com/pentagonum/serial-ws2812b))
* Laser cutted box, see [Fusion360 Project](https://a360.co/2OofzC5)

## Pictures

### Controller

![Controller](pictures/Controller.jpg)

### Lamps

#### Inside

![Inside](pictures/Lamp Inside.jpg)

#### Outside

![Outside](pictures/Lamp Outside.jpg)
